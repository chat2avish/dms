image: docker:latest
services:
  - docker:dind
variables:
  DOCKER_DRIVER: overlay
  SPRING_PROFILES_ACTIVE: gitlab-ci
stages:
  - build
  - stage
  - package
maven-test:
  image: maven:3-jdk-11
  stage: build
  script:
    - mvn test
    - mvn verify
    - mvn jacoco:report
  artifacts:
    reports:
      junit:
        - target/surefire-reports/TEST-*.xml
        - target/failsafe-reports/TEST-*.xml
        - target/site/jacoco/index.html
        - target/site/jacoco/jacoco.xml
maven-satge:
  image: maven:3-jdk-11
  stage: stage
  when: on_success
  only:
    - tags
  script: "mvn package"
  artifacts:
    paths:
      - target/*.jar
docker-build:
  stage: package
  when: on_success
  only:
    - tags
  script:
    - docker build -t registry.gitlab.com/chat2avish/dms .
    - docker login -u gitlab-ci-token -p $CI_BUILD_TOKEN registry.gitlab.com
    - docker push registry.gitlab.com/chat2avish/dms